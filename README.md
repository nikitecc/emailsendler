Приложение на Spring Boot, реализующее функционал отправки письма с содержанием «Пора пить чай!» по списку адресатов каждый день с понедельника по пятницу в 17:00. 

- [Главная страница(скрин)](https://imgur.com/L7RANKF)

- [Страница создания рассылки(скрин)](https://imgur.com/thOQ9zq)

- [Авторизация(скрин)](https://imgur.com/kOBbssk)

- [Регистарция(скрин)](https://imgur.com/29RwutK)

# 1) Docker guide

**Устанавливаем докер**

- [Docker](https://www.docker.com/)

- [Образ на Docker-hub](https://hub.docker.com/r/nikitecc/emailsendler)

Скачать образ
> docker pull nikitecc/emailsendler

**Скачать репозиторий(стабильно работающая ветка - develop)**
- https://gitlab.com/nikitecc/emailsendler.git
               
----
Maven компиляция и установка jar файла   
> mvn clean compile install

Автоматическая установка и сборка в контейнер  
> docker-compose up

Остановка и удаления контейнера
> docker-compose dowm

# 2) Kubernetes guide

**Устанавливаем kubectl**
[kubectl](https://kubernetes.io/docs/tasks/tools/#kubectl)

**Устанавливаем kind**
[kind](https://kind.sigs.k8s.io/docs/user/quick-start/)

**Ingress NGINX**
> kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml

----
Создание кластера
> kind create cluster --config kind-config.yml

Посмотреть созданный кластер, ноды
> kind get cluster, nodes

Удалить кластер
> kind delete cluster

Создание сервиса и пода для базы данных
> kind apply -f k8s/db-service.yaml,k8s/db-deployment.yaml

Создание сервиса и подов для приложения
> kind apply -f k8s/emailsendler-service.yaml,k8s/emailsendler-deployment.yaml

Посмотреть созданные сервисы, поды и развертывания
> kubectl get svc,pods,deployment

Посмотреть информацию о сервисе 
> kubectl describe svc **name_service**

Посмотреть логи пода
> kubectl logs **name_pod** -c **name_container**

Удалить все сервисы, поды, развертывания
> kubectl delete svc,pods,deployment --all
